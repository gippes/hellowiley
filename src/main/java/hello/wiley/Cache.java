package hello.wiley;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static hello.wiley.Cache.Strategy.LFU;
import static hello.wiley.Cache.Strategy.LRU;

public class Cache<K, V> extends LinkedHashMap<K, V> {
    private final Strategy strategy;
    private int maxCapacity;
    private HashMap<K, Integer> keyPriority = null;
    private HashMap<Integer, Set<K>> priorityKeysSet = null;
    private int minPriority = 1;

    public Cache(Strategy strategy, int maxCapacity) {
        super(maxCapacity, 4f, true);
        this.strategy = strategy;
        this.maxCapacity = maxCapacity;
        if (strategy == LFU) {
            keyPriority = new HashMap<>(this.maxCapacity);
            priorityKeysSet = new HashMap<>(this.maxCapacity);
        }
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        if (strategy == LRU) {
            return size() > maxCapacity;
        }
        return false;
    }

    @Override
    public V put(K key, V value) {
        if (strategy == LFU) {
            int priority = minPriority;
            if (containsKey(key)) {
                if (get(key).equals(value)) {
                    return get(key);
                }
                priority = keyPriority.remove(key);
                Set<K> keys = priorityKeysSet.get(priority);
                keys.remove(key);
                priority++;
            }

            if (size() >= maxCapacity) {
                Set<K> minPriorityKeys = priorityKeysSet.get(minPriority);
                K keyToRemove = minPriorityKeys.iterator().next();
                minPriorityKeys.remove(keyToRemove);
                keyPriority.remove(keyToRemove);
                super.remove(keyToRemove);
                if (minPriorityKeys.size() == 0) {
                    priorityKeysSet.remove(minPriority);
                }
            }

            keyPriority.put(key, priority);
            Set<K> keys = priorityKeysSet.get(priority);
            if (keys == null) {
                keys = new HashSet<>();
            }
            keys.add(key);
            priorityKeysSet.put(priority, keys);
        }
        return super.put(key, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public V get(Object key) {
        V returnValue = super.get(key);
        if (returnValue != null) {
            if (strategy == LFU) {
                Integer priority = keyPriority.get(key);
                if (priority == minPriority && priorityKeysSet.get(priority).isEmpty()) {
                    minPriority++;
                }
                priorityKeysSet.get(priority).remove((K) key);
                if (priorityKeysSet.get(priority).isEmpty()) {
                    priorityKeysSet.remove(priority);
                }
                priority++;
                if (priorityKeysSet.get(priority) == null) {
                    Set<K> keys = new HashSet<>();
                    keys.add((K) key);
                    priorityKeysSet.put(priority, keys);
                }
                keyPriority.put((K) key, priority);
            }
        }
        return returnValue;
    }

    public enum Strategy {
        LRU, LFU
    }
}
