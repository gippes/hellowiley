package hello.wiley;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static hello.wiley.Cache.Strategy.LFU;
import static hello.wiley.Cache.Strategy.LRU;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CacheTest {

    private Cache<String, Integer> cache = null;

    @Test
    void maxCapacityCantBeChange() {
        cache = new Cache<>(LFU, 3);

        assertEquals(0, cache.size());
        cache.put("1", 0);
        cache.put("2", 0);
        cache.put("3", 0);
        assertEquals(3, cache.size());
        cache.put("4", 0);
        cache.get("3");
        assertEquals(3, cache.size());
    }

    @Test
    void removeObjectsFromCacheByLruStrategy(){
        cache = new Cache<>(LRU, 3);

        cache.put("1", 0);
        cache.put("2", 0);
        cache.put("3", 0);
        cache.put("4", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("2", "3", "4"))));

        cache.get("3");
        cache.put("5", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("3", "4", "5"))));

        cache.put("6", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("3", "5", "6"))));
    }


    @Test
    void removeObjectsFromCacheByLfuStrategy(){
        cache = new Cache<>(LFU, 3);

        cache.put("1", 0);
        cache.put("2", 0);
        cache.put("3", 0);
        cache.put("4", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("2", "3", "4"))));

        cache.get("2");
        cache.get("2");
        cache.put("5", 0);
        cache.put("6", 0);
        cache.put("7", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("2", "6", "7"))));

        cache.put("3", 0);
        cache.put("6", 0);
        cache.put("7", 0);
        assertThat(cache.keySet(), is(new HashSet<>(asList("2", "6", "7"))));
    }
}